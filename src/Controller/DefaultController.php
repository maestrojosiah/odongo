<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\EducationRepository;
use App\Repository\WorkexperienceRepository;
use App\Repository\UserRepository;
use App\Repository\ProjectsRepository;
use App\Repository\MymusicRepository;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="default")
     */
    public function index(UserRepository $userRepo, EducationRepository $educationRepo, WorkexperienceRepository $workExpRepo, ProjectsRepository $projectsRepo, MymusicRepository $mymusicRepo): Response
    {
        
        $admin = $userRepo->findOneByUsertype('admin');
        $educations = $educationRepo->findAll();
        $works = $workExpRepo->findAll();
        $projects = $projectsRepo->findAll();
        $mymusic = $mymusicRepo->findAll();

        return $this->render('default/index.html.twig', [
            'admin' => $admin,
            'educations' => $educations,
            'workexperiences' => $works,
            'projects' => $projects,
            'mymusic' => $mymusic,
        ]);
    }
}
