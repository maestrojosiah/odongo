<?php

namespace App\Controller;

use App\Entity\Mymusic;
use App\Form\MymusicType;
use App\Repository\MymusicRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * @Route("/mymusic")
 */
class MymusicController extends AbstractController
{
    /**
     * @Route("/", name="mymusic_index", methods={"GET"})
     */
    public function index(MymusicRepository $mymusicRepository): Response
    {
        return $this->render('mymusic/index.html.twig', [
            'mymusics' => $mymusicRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="mymusic_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger): Response
    {
        $mymusic = new Mymusic();
        $form = $this->createForm(MymusicType::class, $mymusic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /** @var uploadedFile1 $uploadedFile1 */
            $uploadedFile1 = $form->get('image')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile1) {
                $originalFilename = pathinfo($uploadedFile1->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile1->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile1->move(
                        $this->getParameter('image_files'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFile1name' property to store the PDF file name
                // instead of its contents
                $mymusic->setImage($newFilename);
            }

            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form->get('audio')->getData();

            // this condition is needed because the 'brochure' field is not required
            // so the PDF file must be processed only when a file is uploaded
            if ($uploadedFile) {
                $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$uploadedFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $uploadedFile->move(
                        $this->getParameter('audio_files'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'uploadedFilename' property to store the PDF file name
                // instead of its contents
                $mymusic->setAudio($newFilename);
            }
            
            $mymusic->setUser($this->getUser());
            $mymusic->setAddedOn(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($mymusic);
            $entityManager->flush();

            return $this->redirectToRoute('mymusic_index');
        }

        return $this->render('mymusic/new.html.twig', [
            'mymusic' => $mymusic,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mymusic_show", methods={"GET"})
     */
    public function show(Mymusic $mymusic): Response
    {
        return $this->render('mymusic/show.html.twig', [
            'mymusic' => $mymusic,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="mymusic_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Mymusic $mymusic): Response
    {
        $form = $this->createForm(MymusicType::class, $mymusic);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('mymusic_index');
        }

        return $this->render('mymusic/edit.html.twig', [
            'mymusic' => $mymusic,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="mymusic_delete", methods={"POST"})
     */
    public function delete(Request $request, Mymusic $mymusic): Response
    {
        if ($this->isCsrfTokenValid('delete'.$mymusic->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($mymusic);
            $entityManager->flush();
        }

        return $this->redirectToRoute('mymusic_index');
    }
}
