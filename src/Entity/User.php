<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $fname;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $lname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $career;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smfb;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smtw;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smli;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $smin;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $about;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $usertype;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $age;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $resume;

    /**
     * @ORM\OneToMany(targetEntity=Mymusic::class, mappedBy="user")
     */
    private $mymusics;

    /**
     * @ORM\OneToMany(targetEntity=Specialty::class, mappedBy="user")
     */
    private $specialties;

    private $fullname;

    public function __construct()
    {
        $this->blogposts = new ArrayCollection();
        $this->mymusics = new ArrayCollection();
        $this->specialties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFullname(): ?string
    {
        return $this->fname . " " . $this->lname;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }

    public function getFname(): ?string
    {
        return $this->fname;
    }

    public function setFname(string $fname): self
    {
        $this->fname = $fname;

        return $this;
    }

    public function getLname(): ?string
    {
        return $this->lname;
    }

    public function setLname(string $lname): self
    {
        $this->lname = $lname;

        return $this;
    }

    public function getCareer(): ?string
    {
        return $this->career;
    }

    public function setCareer(string $career): self
    {
        $this->career = $career;

        return $this;
    }

    public function getSmfb(): ?string
    {
        return $this->smfb;
    }

    public function setSmfb(?string $smfb): self
    {
        $this->smfb = $smfb;

        return $this;
    }

    public function getSmtw(): ?string
    {
        return $this->smtw;
    }

    public function setSmtw(?string $smtw): self
    {
        $this->smtw = $smtw;

        return $this;
    }

    public function getSmli(): ?string
    {
        return $this->smli;
    }

    public function setSmli(?string $smli): self
    {
        $this->smli = $smli;

        return $this;
    }

    public function getSmin(): ?string
    {
        return $this->smin;
    }

    public function setSmin(?string $smin): self
    {
        $this->smin = $smin;

        return $this;
    }

    public function getAbout(): ?string
    {
        return $this->about;
    }

    public function setAbout(?string $about): self
    {
        $this->about = $about;

        return $this;
    }

    public function getUsertype(): ?string
    {
        return $this->usertype;
    }

    public function setUsertype(string $usertype): self
    {
        $this->usertype = $usertype;

        return $this;
    }

    public function getAge(): ?string
    {
        return $this->age;
    }

    public function setAge(?string $age): self
    {
        $this->age = $age;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getResume(): ?string
    {
        return $this->resume;
    }

    public function setResume(?string $resume): self
    {
        $this->resume = $resume;

        return $this;
    }

    /**
     * @return Collection|Mymusic[]
     */
    public function getMymusics(): Collection
    {
        return $this->mymusics;
    }

    public function addMymusic(Mymusic $mymusic): self
    {
        if (!$this->mymusics->contains($mymusic)) {
            $this->mymusics[] = $mymusic;
            $mymusic->setUser($this);
        }

        return $this;
    }

    public function removeMymusic(Mymusic $mymusic): self
    {
        if ($this->mymusics->removeElement($mymusic)) {
            // set the owning side to null (unless already changed)
            if ($mymusic->getUser() === $this) {
                $mymusic->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Specialty[]
     */
    public function getSpecialties(): Collection
    {
        return $this->specialties;
    }

    public function addSpecialty(Specialty $specialty): self
    {
        if (!$this->specialties->contains($specialty)) {
            $this->specialties[] = $specialty;
            $specialty->setUser($this);
        }

        return $this;
    }

    public function removeSpecialty(Specialty $specialty): self
    {
        if ($this->specialties->removeElement($specialty)) {
            // set the owning side to null (unless already changed)
            if ($specialty->getUser() === $this) {
                $specialty->setUser(null);
            }
        }

        return $this;
    }
}
