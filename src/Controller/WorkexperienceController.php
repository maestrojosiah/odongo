<?php

namespace App\Controller;

use App\Entity\Workexperience;
use App\Form\WorkexperienceType;
use App\Repository\WorkexperienceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/workexperience")
 */
class WorkexperienceController extends AbstractController
{
    /**
     * @Route("/", name="workexperience_index", methods={"GET"})
     */
    public function index(WorkexperienceRepository $workexperienceRepository): Response
    {
        return $this->render('workexperience/index.html.twig', [
            'workexperiences' => $workexperienceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="workexperience_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $workexperience = new Workexperience();
        $form = $this->createForm(WorkexperienceType::class, $workexperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($workexperience);
            $entityManager->flush();

            return $this->redirectToRoute('workexperience_index');
        }

        return $this->render('workexperience/new.html.twig', [
            'workexperience' => $workexperience,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="workexperience_show", methods={"GET"})
     */
    public function show(Workexperience $workexperience): Response
    {
        return $this->render('workexperience/show.html.twig', [
            'workexperience' => $workexperience,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="workexperience_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Workexperience $workexperience): Response
    {
        $form = $this->createForm(WorkexperienceType::class, $workexperience);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('workexperience_index');
        }

        return $this->render('workexperience/edit.html.twig', [
            'workexperience' => $workexperience,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="workexperience_delete", methods={"POST"})
     */
    public function delete(Request $request, Workexperience $workexperience): Response
    {
        if ($this->isCsrfTokenValid('delete'.$workexperience->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($workexperience);
            $entityManager->flush();
        }

        return $this->redirectToRoute('workexperience_index');
    }
}
