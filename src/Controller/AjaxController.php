<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Service\Mailer;
use Twig\Environment;
use App\Repository\UserRepository;
use Knp\Snappy\Pdf;
use App\Entity\Specialty;

class AjaxController extends AbstractController
{
    private $snappy_pdf;

    public function __construct(Pdf $snappy_pdf){
        $this->snappy_pdf = $snappy_pdf;
    }    

    /**
     * @Route("/ajax", name="ajax")
     */
    public function index(): Response
    {
        return $this->render('ajax/index.html.twig', [
            'controller_name' => 'AjaxController',
        ]);
    }

    /**
     * @Route("/homepage/contact/form", name="contactformsend")
     */
    public function contactForm(Request $request, Mailer $mailer, Environment $twig, UserRepository $userRepo): Response
    {        
        
        $fname = $request->request->get('fname');
        $lname = $request->request->get('lname');
        $email = $request->request->get('email');
        $subject = $request->request->get('subject');
        $message = $request->request->get('message');
        $maildata = ['name' => $fname.' '.$lname, 'emailAd' => $email, 'subject' => $subject, 'message' => $message];

        $admins = $userRepo->findByUsertype('admin');
        
        $mailer->sendEmailMessage($maildata, 'contact@odongoaustine.com', $subject, "contactform.html.twig", "necessary", "contact_form");    
        
        $mailer->sendEmailMessage($maildata, $email, "Your message - ". $subject . " - received", "contactreceived.html.twig", "necessary", "contact_form");    

        
        $this->addFlash(
            'success',
            'The message was sent successfully.',
        );
        return new JsonResponse( 'test' );

    }

    /**
     * @Route("/upload/profile/picture", name="save_profile_picture")
     */
    public function saveFile(Request $request)
    {
        $file = $_POST;
        $path_to_save = 'profile_img_directory';
        if($file){

            $current_photo_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().".png";
            $short_profile_img_directory = $this->getParameter('short_profile_img_directory').time().$this->getUser()->getId().".png";
            $data = $_POST['image'];
            $image_array_1 = explode(";", $data);
            $image_array_2 = explode(",", $image_array_1[1]);
            $data = base64_decode($image_array_2[1]);
            file_put_contents($current_photo_path, $data);

            $user = $this->getUser();

            $prefix_to_delete = $this->getParameter('prefix_to_delete');
            $existing_img = $prefix_to_delete . $user->getPhoto();
            if(is_file($existing_img) && file_exists($existing_img)){ unlink($existing_img); }

            $user->setPhoto($short_profile_img_directory);
            $this->save($user);             
            return new JsonResponse($short_profile_img_directory);
            
        }
        // return new JsonResponse("success");
    }

    /**
     * @Route("/save/user/bio", name="savebio")
     */
    public function savebio(Request $request): Response
    {
        $user = $this->getUser();

        $phone = $request->request->get('phone');
        $fname = $request->request->get('fname');
        $lname = $request->request->get('lname');
        $email = $request->request->get('email');
        $about = $request->request->get('about');

        $user->setFname($fname);
        $user->setLname($lname);
        $user->setPhone($phone);
        $user->setEmail($email);
        $user->setAbout($about);

        $this->save($user);


        return new JsonResponse($_POST);

    }

    /**
     * @Route("/save/user/skill", name="saveskill")
     */
    public function saveskill(Request $request): Response
    {
        $user = $this->getUser();

        $title = $request->request->get('title');
        $level = $request->request->get('level');
        
        $specialty = new Specialty();

        $specialty->setTitle($title);
        $specialty->setLevel($level);
        $specialty->setUser($user);

        $this->save($specialty);

        return new JsonResponse($level);

    }

    /**
     * @Route("/save/user/extras", name="saveextras")
     */
    public function saveExtras(Request $request): Response
    {
        $user = $this->getUser();

        $path_to_save = 'resume_directory';

        $career = $request->request->get('career');
        $smfb = $request->request->get('smfb');
        $smtw = $request->request->get('smtw');
        $smli = $request->request->get('smli');
        $smin = $request->request->get('smin');


        $current_resume_path = $this->getParameter($path_to_save).time().$this->getUser()->getId().".pdf";
        $short_resume_directory = $this->getParameter('short_resume_directory').time().$this->getUser()->getId().".pdf";
        $data = $_FILES['resume']['tmp_name'];
        $filename = $_FILES['resume']['name'];
        // file_put_contents($current_resume_path, $data);
        move_uploaded_file($data, $current_resume_path);

        $prefix_to_delete = $this->getParameter('prefix_to_delete');
        $existing_resume = $prefix_to_delete . $user->getResume();
        if(is_file($existing_resume) && file_exists($existing_resume)){ unlink($existing_resume); }

        $user->setResume($short_resume_directory);
        $user->setSmfb($smfb);
        $user->setSmtw($smtw);
        $user->setSmli($smli);
        $user->setSmin($smin);
        $user->setCareer($career);

        $this->save($user);             


        return new JsonResponse($_FILES);

    }

    public function save($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
    }

    public function delete($entity){
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
    }

}
