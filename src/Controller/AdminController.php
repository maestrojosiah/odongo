<?php

namespace App\Controller;

use App\Repository\EducationRepository;
use App\Repository\MymusicRepository;
use App\Repository\SpecialtyRepository;
use App\Repository\WorkexperienceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(EducationRepository $educationRepo, WorkexperienceRepository $workRepo, SpecialtyRepository $specialtyRepo, MymusicRepository $mymusicRepo): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');

        $edu = $educationRepo->findAll();
        $works = $workRepo->findAll();
        $skills = $specialtyRepo->findAll();
        $mymusic = $mymusicRepo->findAll();

        return $this->render('admin/index.html.twig', [
            'edu' => $edu,
            'works' => $works,
            'skills' => $skills,
            'mymusic' => $mymusic,
        ]);
    }


    /**
     * @Route("/admin/profile", name="profile")
     */
    public function profile(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_REMEMBERED');
        $admin = $this->getUser();
        return $this->render('admin/profile.html.twig', [
            'admin' => $admin,
        ]);
    }
}
