<?php

namespace App\Repository;

use App\Entity\Workexperience;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Workexperience|null find($id, $lockMode = null, $lockVersion = null)
 * @method Workexperience|null findOneBy(array $criteria, array $orderBy = null)
 * @method Workexperience[]    findAll()
 * @method Workexperience[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkexperienceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Workexperience::class);
    }

    // /**
    //  * @return Workexperience[] Returns an array of Workexperience objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('w.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Workexperience
    {
        return $this->createQueryBuilder('w')
            ->andWhere('w.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
