<?php

namespace App\Repository;

use App\Entity\Mymusic;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Mymusic|null find($id, $lockMode = null, $lockVersion = null)
 * @method Mymusic|null findOneBy(array $criteria, array $orderBy = null)
 * @method Mymusic[]    findAll()
 * @method Mymusic[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MymusicRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mymusic::class);
    }

    // /**
    //  * @return Mymusic[] Returns an array of Mymusic objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Mymusic
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
