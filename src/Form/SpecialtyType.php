<?php

namespace App\Form;

use App\Entity\Specialty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SpecialtyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('level', ChoiceType::class, [
                'choices' => [
                    'Grade 1' => '1',
                    'Grade 2' => '2',
                    'Grade 3' => '2',
                    'Grade 4' => '3',
                    'Grade 5' => '4',
                    'Grade 6' => '5',
                    'Grade 7' => '7',
                    'Grade 8' => '8',
                    'Diploma' => 'Dip',
                ],
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Specialty::class,
        ]);
    }
}
