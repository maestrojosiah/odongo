<?php

namespace App\Entity;

use App\Repository\WorkexperienceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=WorkexperienceRepository::class)
 */
class Workexperience
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $startyr;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $endyr;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $jobtitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $companyname;

    /**
     * @ORM\Column(type="text")
     */
    private $roles;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartyr(): ?string
    {
        return $this->startyr;
    }

    public function setStartyr(string $startyr): self
    {
        $this->startyr = $startyr;

        return $this;
    }

    public function getEndyr(): ?string
    {
        return $this->endyr;
    }

    public function setEndyr(string $endyr): self
    {
        $this->endyr = $endyr;

        return $this;
    }

    public function getJobtitle(): ?string
    {
        return $this->jobtitle;
    }

    public function setJobtitle(string $jobtitle): self
    {
        $this->jobtitle = $jobtitle;

        return $this;
    }

    public function getCompanyname(): ?string
    {
        return $this->companyname;
    }

    public function setCompanyname(string $companyname): self
    {
        $this->companyname = $companyname;

        return $this;
    }

    public function getRoles(): ?string
    {
        return $this->roles;
    }

    public function setRoles(string $roles): self
    {
        $this->roles = $roles;

        return $this;
    }
}
